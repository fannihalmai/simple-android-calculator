package com.example.calculator;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    int a;
    int b;
    String operator;
    String operationFull;
    int counter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void calculate(){
        TextView answer = findViewById(R.id.answer);
        TextView operation = findViewById(R.id.operation);
        long res = 0;
        String[] parts = operationFull.split(operator);
        a = Integer.parseInt(parts[0]);
        b= Integer.parseInt(parts[1]);

        switch (operator) {
            case "\\+":
                res = a + b;
                break;
            case "-":
                res = a - b;
                break;
            case "/":
                res = a / b;
                break;
            case "\\*":
                res = a * b;
                break;
        }
        answer.setText(String.valueOf(res));
        a = (int) res;
        operationFull = String.valueOf(res);
        operation.setText(operationFull);
    }

    public void handleButtonClick(View v){
        String keystroke = "";
        switch (v.getId()) {
            case R.id.b0:
                keystroke = "0";
                break;
            case R.id.b1:
                keystroke = "1";
                break;
            case R.id.b2:
                keystroke = "2";
                break;
            case R.id.b3:
                keystroke = "3";
                break;
            case R.id.b4:
                keystroke = "4";
                break;
            case R.id.b5:
                keystroke = "5";
                break;
            case R.id.b6:
                keystroke = "6";
                break;
            case R.id.b7:
                keystroke = "7";
                break;
            case R.id.b8:
                keystroke = "8";
                break;
            case R.id.b9:
                keystroke = "9";
                break;
            case R.id.mult:
                operator = "\\*";
                keystroke = "*";
                break;
            case R.id.sub:
                operator = "-";
                keystroke = "-";
                break;
            case R.id.div:
                operator = "/";
                keystroke = "/";
                break;
            case R.id.add:
                operator = "\\+";
                keystroke = "\\+";
                break;
            case R.id.equal:
                calculate();
            default:
                break;
        }
        operationFull = operationFull == null ? keystroke : operationFull + keystroke;
        TextView operation = findViewById(R.id.operation);
        operation.setText(String.valueOf(operationFull));
    }
}

